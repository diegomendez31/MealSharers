# MealSharers Application


## Description

This a platform based on: 

- C#
- Visual Studio

### Modelling

![alt text](https://lh5.googleusercontent.com/GZYd_NZ2_z0i_Wl9paJr_JFlPSRGLt2MOhurCrAX1VrKJPSFe5kFvJD4wrmeyhzz_NityxH25aAxWA=w2560-h1247-rw)

### Prototype

The proposed current prototype is based on the requirements identified in the first stage of application development. The Meal Sharers system is divided by the four types of users who have different functionalities. Although users with the role of administrator and manager have functions in common, the current prototype makes the difference between them, allowing management functionalities depending of each role. The functions of "List critical cooks", "List satisfaction rating Eaters", "Volunteer map" and "Generate quarterly report" allow the management of the Meal Sharers information system in a simple and scalable way, adapting to the number of users within the system.
On the other hand, the functionality for an Eater user is based on the ease with which it can manage and accept meals that are proposed. The application allows a user to register easily and indicates if there is an error with the required information. On the main page of the Eater, the functionality is displayed in buttons, which allows it to interact with Cooks and meals. The profile allows the user to be aware of its rating and personal information. The meals and invitations list give an overview of the two basic functionalities for an Eater: Accept a meal and review. As with the administrative part, the system is adapted to the number of users, collecting information and grouping it into collections belonging to each user.
For the Cook user, the interaction with the system is more detailed. This must interact with external systems that the prototype currently simulates in order to obtain the hygiene certificate and the PVG criminal record check. However, the prototype of the application generates and creates the user considering the different Cook states. The functionality for this type of user is similar to the Eater. Nevertheless, the user profile should be more detailed. In the solution, the user is allowed to list their reviews and improve their experience within the volunteer program. The list of your meals is based on the same system-wide review model, but the basic functionality of the Cook, "Offer a meal" is based on the Eaters users of the system. As the prototype has data that is stored in collections such as SortedList or List, the search for users is effective and again, it becomes scalable to the number of users that the system could handle.
In conclusion, the current prototype of the system meets the basic requirements previously proposed for operation and scalability. However, as future work, it could consider improving some functionalities, such as adapting the Google Maps API for volunteer users, so they can manage meals and meetings interactively. On the other hand, privacy management should be considered from a security point of view. The current security management is done at a basic level, which allows gaps in information and therefore, allow data disclosure. Privacy by design as the main framework during the development process should provide a holistic view of the system and the personal information handling.
